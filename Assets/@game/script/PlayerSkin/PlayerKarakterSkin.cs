﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player Skin Data", fileName = "PlayerKarakterSkin")]
public class PlayerKarakterSkin : ScriptableObject
{
    public string rambut;
    public int indexBaju;
    public int indexCelana;

    public void SetStringRambut(string labelRambut)
    {
        rambut = labelRambut;
    }

    public void SetIndexBaju(int indeks)
    {
        indexBaju = indeks;
    }

    public void SetIndexCelana(int indeks)
    {
        indexCelana = indeks;
    }
}
