﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Fungus;

[CommandInfo("Scene",
             "Load Scene Async",
             "Load scene asynchronously untuk menampilkan loading progress.")]
public class LoadSceneAsyncCommand : Command
{
    [SerializeField] private string _namaScene;
    [SerializeField] private GameObject _loadingScreen;

    public override void OnEnter()
    {
        Debug.Log(_namaScene);

        // munculkan & play animasi loading screen
        // tepat sebelum mulai load scene baru secara async
        // _loadingScreen.SetActive(true);

        StartCoroutine(LoadSceneDenganProgress(_namaScene));
    }

    public override string GetSummary()
    {
        if (_namaScene == "")
        {
            return "Error: Scene belum diset!";
        }

        return _namaScene;
    }

    public override Color GetButtonColor()
    {
        return new Color32(193, 248, 44, 255);
    }

    IEnumerator LoadSceneDenganProgress(string namaScene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(namaScene);

        while (!operation.isDone)
        {
            Debug.Log(operation.progress);

            yield return null;
        }
    }
}
