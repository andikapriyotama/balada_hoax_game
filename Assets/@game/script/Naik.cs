﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Naik : MonoBehaviour
{
    public void naikin1opsi()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector3(13,146,0);
    }

    public void naikin2opsi()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector3(13,291,0);
    }

    public void naikin3opsi()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector3(13,416,0);
    }

    public void naikin4opsi()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector3(13,461,0);
    }
    

    public void resetPosisi()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector3(13,41,0);
    }
}
