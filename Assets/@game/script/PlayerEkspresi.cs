﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEkspresi : MonoBehaviour
{
    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public void IdleKedip()
    {
        anim.Play("dude-kedip_idle");
    }

    public void EkspresiWoo()
    {
        anim.Play("Dude-Woo");
    }

    public void EkspresiKaget()
    {
        anim.Play("Dude-Kaget");
    }

    public void EkspresiNangis()
    {
        anim.Play("Dude-Nangis");
    }

    public void PosturDitangkap()
    {
        anim.Play("Dude-Ditangkap");
    }

    public void PosturIdle()
    {
        anim.Play("dude-idle");
    }
}
