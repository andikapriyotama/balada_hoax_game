﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableObjectDataPlayer : ScriptableObject
{

    public string namaPlayer;
    public bool cowok;
    public DataPakaianCustom DataPakaianCustom;
}

[Serializable]
public class DataPakaianCustom
{
    public string labelBaju;
    public string labelCelana;
}
