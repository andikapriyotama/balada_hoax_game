﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIDariDataSO : MonoBehaviour
{
    [SerializeField] private ScriptableObjectDataPlayer dataPlayer;
    [SerializeField] private TextMeshProUGUI UITeks;
    [SerializeField] private TMP_InputField input;

    void Start()
    {
        // UITeks.text = "haha";
        UITeks.text = dataPlayer.namaPlayer;
    }

    public void SetNamaZacky()
    {
        dataPlayer.namaPlayer = "Zacky";
        UITeks.text = dataPlayer.namaPlayer;
    }

    public void SimpanDataInputNama()
    {
        dataPlayer.namaPlayer = input.text;
    }
}
