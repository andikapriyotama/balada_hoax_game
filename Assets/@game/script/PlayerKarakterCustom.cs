﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;
using Fungus;

public class PlayerKarakterCustom : MonoBehaviour
{
    [SerializeField] private PlayerKarakterSkin _skinData;

    [SerializeField] private SpriteLibraryAsset _spriteLibSkin;

    // =============================================
    [Header("Referensi ke SpriteResolver")]
    [SerializeField] private SpriteResolver _resolverHair;
    [SerializeField] private List<SpriteResolver> _resolverBaju;
    [SerializeField] private List<SpriteResolver> _resolverCelana;
    // =============================================
    // List label Sprite Renderer skin yang ada di library
    // untuk referensi pemanggilan/swapping sprite
    private List<string> _listLabelHair;
    // ---------------------------------------------
    private List<string> _listLabelShirt;
    private List<string> _listLabelTanganAtasL;
    private List<string> _listLabelTanganBawahL;
    private List<string> _listLabelTanganAtasR;
    private List<string> _listLabelTanganBawahR;
    // ---------------------------------------------
    private List<string> _listLabelButt;
    private List<string> _listLabelKakiAtasL;
    private List<string> _listLabelKakiBawahL;
    private List<string> _listLabelKakiAtasR;
    private List<string> _listLabelKakiBawahR;
    // =============================================

    void Awake()
    {
        // _spriteLibSkin = GetComponentInParent<SpriteLibrary>().spriteLibraryAsset;
        // // TODO: comment/hilangkan saja, cukup pakai drag-drop lewat inspector
        // // ====================================================================
        // // Get & set SpriteResolver dari "hair"
        // if (_resolverHair == null)
        //     _resolverHair = transform.Find("hair").GetComponent<SpriteResolver>();
        // // ====================================================================

        _getListLabelHair();
        _getListLabelBaju();
        _getListLabelCelana();
    }

    void Start()
    {
        // =============================================
        // Setiap awal scene/awal objek karakter aktif,
        // skin mulai diimplemen
        _setCustomSkin(_skinData);
        // =============================================
        
    }

    // =============================================
    // PRIVATE
    // =============================================
    // Get semua nama label sprite "hair" sebagai string,
    // simpan dalam List
    private void _getListLabelHair()
    {
        _listLabelHair = new List<string>();
        foreach (string stringLabelHair in _spriteLibSkin.GetCategoryLabelNames("hair"))
        {
            _listLabelHair.Add(stringLabelHair);
        }
    }

    private void _getListLabelBaju()
    {
        _listLabelShirt = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("shirt"))
        {
            _listLabelShirt.Add(stringLabel);
        }
        
        _listLabelTanganAtasL = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("tangan atas L"))
        {
            _listLabelTanganAtasL.Add(stringLabel);
        }

        _listLabelTanganBawahL = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("tangan bawah L"))
        {
            _listLabelTanganBawahL.Add(stringLabel);
        }
        
        _listLabelTanganAtasR = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("tangan atas R"))
        {
            _listLabelTanganAtasR.Add(stringLabel);
        }

        _listLabelTanganBawahR = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("tangan bawah R"))
        {
            _listLabelTanganBawahR.Add(stringLabel);
        }
    }

    private void _getListLabelCelana()
    {
        _listLabelButt = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("butt"))
        {
            _listLabelButt.Add(stringLabel);
        }
        
        _listLabelKakiAtasL = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("kaki atas L"))
        {
            _listLabelKakiAtasL.Add(stringLabel);
        }

        _listLabelKakiBawahL = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("kaki bawah L"))
        {
            _listLabelKakiBawahL.Add(stringLabel);
        }
        
        _listLabelKakiAtasR = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("kaki atas R"))
        {
            _listLabelKakiAtasR.Add(stringLabel);
        }

        _listLabelKakiBawahR = new List<string>();
        foreach (string stringLabel in _spriteLibSkin.GetCategoryLabelNames("kaki bawah R"))
        {
            _listLabelKakiBawahR.Add(stringLabel);
        }
    }

    // set sprite karakter custom dari
    // variabel global yang ada di scene sebelumnya (menu)
    private void _setCustomSkin(PlayerKarakterSkin skin)
    {
        Debug.Log("Implement skin ke karakter!");

        // ==========================================
        // Cek awal misal masih kosongan SO-nya
        if (skin.rambut == "")
            skin.SetStringRambut(_listLabelHair[0]);
        // ==========================================

        _resolverHair.SetCategoryAndLabel("hair", skin.rambut);
        // ------------------------------------------------------------
        _resolverBaju[0].SetCategoryAndLabel("shirt", _listLabelShirt[skin.indexBaju]);
        _resolverBaju[1].SetCategoryAndLabel("tangan atas L", _listLabelTanganAtasL[skin.indexBaju]);
        _resolverBaju[2].SetCategoryAndLabel("tangan bawah L", _listLabelTanganBawahL[skin.indexBaju]);
        _resolverBaju[3].SetCategoryAndLabel("tangan atas R", _listLabelTanganAtasR[skin.indexBaju]);
        _resolverBaju[4].SetCategoryAndLabel("tangan bawah R", _listLabelTanganBawahR[skin.indexBaju]);
        // ------------------------------------------------------------
        _resolverCelana[0].SetCategoryAndLabel("butt", _listLabelButt[skin.indexCelana]);
        _resolverCelana[1].SetCategoryAndLabel("kaki atas L", _listLabelKakiAtasL[skin.indexCelana]);
        _resolverCelana[2].SetCategoryAndLabel("kaki bawah L", _listLabelKakiBawahL[skin.indexCelana]);
        _resolverCelana[3].SetCategoryAndLabel("kaki atas R", _listLabelKakiAtasR[skin.indexCelana]);
        _resolverCelana[4].SetCategoryAndLabel("kaki bawah R", _listLabelKakiBawahR[skin.indexCelana]);
    }

    private void _swapSpriteHair(int arah)
    {
        Debug.Log("Ganti rambut!");

        // Indeks sekarang? -- indeks di list label
        int indeks = 0;
        for (int i = 0; i < _listLabelHair.Count; i++)
        {
            if (_listLabelHair[i] == _resolverHair.GetLabel())
            {
                indeks = i;
                break;
            }
        }

        // Indeks berikutnya?
        if (arah > 0)
        {
            indeks++;
            if (indeks >= _listLabelHair.Count)
                indeks = 0;
        }

        if (arah < 0)
        {
            indeks--;
            if (indeks < 0)
                indeks = _listLabelHair.Count - 1;
        }

        Debug.Log("Indeks Sprite hair yang aktif: " + indeks);

        // Set Sprite berdasarkan labelnya!
        _resolverHair.SetCategoryAndLabel("hair", _listLabelHair[indeks]);
        
        // Simpan label di skin data
        _skinData.SetStringRambut(_listLabelHair[indeks]);
    }

    private void _swapSpriteBaju(int arah)
    {
        Debug.Log("Ganti Baju!");

        // Indeks sekarang? -- indeks di list label
        int indeks = 0;
        for (int i = 0; i < _listLabelShirt.Count; i++)
        {
            if (_listLabelShirt[i] == _resolverBaju[0].GetLabel())
            {
                indeks = i;
                Debug.Log("indeks: " + indeks);
                break;
            }
        }

        // Indeks berikutnya?
        if (arah > 0)
        {
            indeks++;
            if (indeks >= _listLabelShirt.Count)
                indeks = 0;
        }

        if (arah < 0)
        {
            indeks--;
            if (indeks < 0)
                indeks = _listLabelShirt.Count - 1;
        }

        // Set Sprite berdasarkan labelnya!
        _resolverBaju[0].SetCategoryAndLabel("shirt", _listLabelShirt[indeks]);
        _resolverBaju[1].SetCategoryAndLabel("tangan atas L", _listLabelTanganAtasL[indeks]);
        _resolverBaju[2].SetCategoryAndLabel("tangan bawah L", _listLabelTanganBawahL[indeks]);
        _resolverBaju[3].SetCategoryAndLabel("tangan atas R", _listLabelTanganAtasR[indeks]);
        _resolverBaju[4].SetCategoryAndLabel("tangan bawah R", _listLabelTanganBawahR[indeks]);
        
        // Simpan indekx label di skin data
        _skinData.SetIndexBaju(indeks);
    }

    private void _swapSpriteCelana(int arah)
    {
        Debug.Log("Ganti Celana!");

        // Indeks sekarang? -- indeks di list label
        int indeks = 0;
        for (int i = 0; i < _listLabelButt.Count; i++)
        {
            if (_listLabelButt[i] == _resolverCelana[0].GetLabel())
            {
                indeks = i;
                Debug.Log("indeks: " + indeks);
                break;
            }
        }

        // Indeks berikutnya?
        if (arah > 0)
        {
            indeks++;
            if (indeks >= _listLabelButt.Count)
                indeks = 0;
        }

        if (arah < 0)
        {
            indeks--;
            if (indeks < 0)
                indeks = _listLabelButt.Count - 1;
        }

        // Set Sprite berdasarkan labelnya!
        _resolverCelana[0].SetCategoryAndLabel("butt", _listLabelButt[indeks]);
        _resolverCelana[1].SetCategoryAndLabel("kaki atas L", _listLabelKakiAtasL[indeks]);
        _resolverCelana[2].SetCategoryAndLabel("kaki bawah L", _listLabelKakiBawahL[indeks]);
        _resolverCelana[3].SetCategoryAndLabel("kaki atas R", _listLabelKakiAtasR[indeks]);
        _resolverCelana[4].SetCategoryAndLabel("kaki bawah R", _listLabelKakiBawahR[indeks]);
        
        // Simpan index label di skin data
        _skinData.SetIndexCelana(indeks);
    }


    // =============================================
    // PUBLIC: handler tombol untuk custom karakter
    // =============================================
    // Hair
    public void SwapHairKiri()
    {
        Debug.Log("Swap kiri!");
        _swapSpriteHair(-1);
    }

    public void SwapHairKanan()
    {
        Debug.Log("Swap kanan!");
        _swapSpriteHair(1);
    }
    // ---------------------------------------
    // Baju
    public void SwapBajuKiri()
    {
        Debug.Log("Swap kiri!");
        _swapSpriteBaju(-1);
    }

    public void SwapBajuKanan()
    {
        Debug.Log("Swap kanan!");
        _swapSpriteBaju(1);
    }
    // ---------------------------------------
    // Celana
    public void SwapCelanaKiri()
    {
        Debug.Log("Swap kiri!");
        _swapSpriteCelana(-1);
    }

    public void SwapCelanaKanan()
    {
        Debug.Log("Swap kanan!");
        _swapSpriteCelana(1);
    }
    // =============================================
}
