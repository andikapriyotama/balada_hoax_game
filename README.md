Source code game visual novel Android **Balada Hoax** untuk keperluan dokumentasi.

Projek Unity dibuat dengan versi **2019.2.1p1** dan perlu terinstall **Android Build Support**. Membuka projek dengan Unity versi lebih baru perlu reimport, yang akibatnya hasil yang mungkin tidak bisa ditebak.

---

## Credit:

- **Game Artist**: Aditya Priyudha *(DKV, Universitas Dian Nuswantoro, 2015)*
- **Game Programmer**: Andika Priyotama D.
